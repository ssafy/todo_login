from django.shortcuts import render, redirect
from .models import Todo
from django.contrib.auth.models import User
# Create your views here.
def home(request):
    # todos에 있는 내용을 다 가져와 보여주기

    todos = request.user.todo_set.all()
    context = {
        'todos': todos
    }
    return render(request, 'todos/home.html', context)

def create(request):
    # todos 작성하기
    content = request.POST.get('content')
    user_id = request.user.id
    # completed = request.POST.get('completed')
    # 현재 접속해 있는 유저의 아이디
    Todo.objects.create(content=content, user_id=user_id)


    return redirect('todos:home')

def check(request, id):
    # 특정 id를 가진 투두를 뽑아 completed = True
    todo = Todo.objects.get(pk=id)
    todo.completed = False if todo.completed else True
    todo.save()
    return redirect('todos:home')